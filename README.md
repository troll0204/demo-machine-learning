# README #

### What is this repository for? ###

#### Tools for Machine Learning of bacterial phenotypes from genomic data

##### Data
The data from Zankari et al. is on computerome in in:

`/home/projects/cge/people/s123580/Sequences/ResFinderData`

The data from Stoesser et as is in:

`/home/projects/cge/people/s123580/Sequences/UKhospital`


The *.tab files contain mic DATA

* 0: Susceptible
* 1: Resistent 
* 2: Inconclusive in repeated tests